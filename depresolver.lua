-- deets: depresolver.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("utils-table")

function tcopy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

function tlen(t)
  local c = 0
  for _,_ in pairs(t) do
    c = c + 1
  end
  return c
end

function tremove(t,val)
  for k,v in pairs(t) do
    if (v == val) then
      t[k] = nil
    end
  end
end

function resolve_deps(deptable)

t = tcopy(deptable)
a = {}
i = 0
oldi = 0

while tlen(t) ~= oldi do
  oldi = i
  i = tlen(t)
  undefined_deps = {}
  notdependent = {}
  newt = {}

  for k,v in pairs(t) do
    if (#v > 0) then
      vset = {}
      for _,z in pairs(v) do
        vset[z] = true
      end
      for y,_ in pairs(t) do
        vset[y] = nil
      end
      for y,_ in pairs(vset) do
        table.insert(undefined_deps, y)
      end
    else
      notdependent[k] = true
      table.insert(a, k)
      t[k] = nil
    end
  end

  for k,v in pairs(t) do
    for y,_ in pairs(notdependent) do
      tremove(v,y)
    end
    if (#v > 0) then
      t[k] = v
    end
  end
end
  return tlen(t), a
end
