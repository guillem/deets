-- deets: debian.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("dpkg")
require("resource")

local ipairs = ipairs
local dpkg = dpkg
local print = print -- remove this in new phase
local register_resource = register_resource

module("debian")

function iscompliant(pkg, gota)
  local desiredstatus = gota.ensure
  local status = dpkg.status(pkg)

  if (desiredstatus == "installed") then
     if (status == "installed") then
        return true
     else
        return false
     end
  elseif (desiredstatus == "purged") then
     if (status == "not-installed") then
        return true
     else
        return false
     end
  elseif (desiredstatus == "removed") then
     if (status == "not-installed" or status == "config-files") then
        return true
     else
        return false
     end
  end

  return false
end

function rectify(pkg, gota)
  local status = gota.ensure
  if (status == "installed") then
	return "aptitude -y install "..pkg
  elseif (status == "removed") then
	return "aptitude -y remove "..pkg
  elseif (status == "purged") then
	return "dpkg -P "..pkg
  end
end

function package(pkglist, gotita)
  for _,pkg in ipairs(pkglist) do
    if (not register_resource("pkg", pkg, gotita)) then
        print("Registration error", pkg)
    end
  end
end
