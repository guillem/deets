-- deets: file.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("deets")
-- require("lfs")
require("md5")

require("resource")
require("cosmo")

local ipairs = ipairs
local register_resource = register_resource
local deets = deets
local table = table
local string = string
local io = io
local md5 = md5
local cosmo = cosmo
local tempdir = tempdir
local print = print -- delete this soonish

module("file")

function checksum(filename)
local f, x

  if (not filename) then return nil end

  f = io.open(filename,"rb")
  if (f) then
    x = f:read("*a")
    f:close()
    return md5.sumhexa(x)
  else
    return nil
  end
end

function file_or_dir(files, gota)
  if (gota.mode and string.find(gota.mode, "[^0-7]")) then
    print("Invalid mode "..gota.mode..", ignoring requirement.")
    gota.mode = nil
  end
  for _,y in ipairs(files) do
    if (not register_resource("file", y, gota)) then
	print("Registration error", y)
    end
  end
end

function file(files, gota)
  gota.fileordir = "file"
  file_or_dir(files, gota)
end

function directory(files, gota)
  gota.fileordir = "directory"
  file_or_dir(files, gota)
end

function iscompliant(file, gota)
--    stat = lfs.symlinkattributes(file)
    local stat = nil
    type, sm, su, sg = deets.tempstat(file)
    if (type) then
      stat = {}
      stat.mode = sm
      stat.uid = su
      stat.gid = sg
    end
    if (gota.ensure == "present") then
      if (not stat) then return false end
      if (type~= gota.fileordir) then return false end
      if (gota.mode and stat.mode ~= gota.mode) then return false end
      if (gota.owner and gota.owner ~= deets.username(stat.uid)) then return false end
      if (gota.group and gota.group ~= deets.groupname(stat.gid)) then return false end
      if (gota.source and (checksum(file) ~= checksum(gota.source))) then
        return false
      end
    elseif (gota.ensure == "absent") then
      if (stat) then return false end
    end

    return true
end

function rectify(file, gota)
--    stat = lfs.symlinkattributes(file)
    local stat = nil
    local steps = {}
    type, sm, su, sg = deets.tempstat(file)
    if (type) then
      stat = {}
      stat.mode = sm
      stat.uid = su
      stat.gid = sg
    end
    if (gota.ensure == "present") then
      if (stat and type~=gota.fileordir) then
	if (gota.fileordir == "file") then
          if (type == "directory") then
          table.insert(steps,"rmdir "..gota.name.."; touch "..gota.name)
          else
          table.insert(steps,"rm -f "..gota.name.."; touch "..gota.name)
          end
        elseif (gota.fileordir == "directory") then
          table.insert(steps,"rm -f "..gota.name.."; mkdir "..gota.name)
        end
      elseif (type == nil) then
        stat = {}
        stat.mode = "fake"
        stat.uid = "fake"
        stat.gid = "fake"
	if (gota.fileordir == "directory") then
          table.insert(steps,"mkdir "..gota.name)
        end
      end
      if (gota.source and (checksum(file) ~= checksum(gota.source))) then
        table.insert(steps,"cp "..gota.source.." "..file)
      end
      if (gota.mode and stat.mode ~= gota.mode) then table.insert(steps,"chmod "..gota.mode.." "..gota.name) end
      if ((not type) or (gota.owner and gota.owner ~= deets.username(stat.uid))) then table.insert(steps,"chown "..gota.owner.." ".. gota.name)  end
      if ((not type) or (gota.group and gota.group ~= deets.groupname(stat.gid))) then table.insert(steps,"chgrp "..gota.group.." ".. gota.name)  end
    elseif (stat and gota.ensure == "absent") then
      if (type == "directory") then table.insert(steps, "rmdir "..gota.name)
      elseif (type == "file") then table.insert(steps, "rm -f "..gota.name) end
    end

  return table.concat(steps, "\n")
end

function cosmotemplate(filename, addlvars)
  local f, x, fill, newname, newfile

  f = io.open(filename,"rb")
  if (f) then
    x = f:read("*a")
    f:close()
    fill = cosmo.fill(x, {systeminfo = deets.systeminfo, map=cosmo.map, inject=cosmo.inject, ["if"] = cosmo.cif, v=addlvars})
    newname = tempdir..string.sub(filename, string.find (filename, "/[^/]+$"))
    newfile = io.open(newname,"wb")
    newfile:write(fill)
    newfile:close()
    return newname
  else
    return nil
  end
end
