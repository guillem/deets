-- deets: utils-grep.lua
-- Copyright (C) 2011-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

function grep(pattern, filehandle)
local lineno = 0
local matches = {}

for line in filehandle:lines() do
	if (string.find(line, pattern) ~= nil) then
		table.insert(matches, line)
	end
end

return matches
end

function grep_program_output(pattern,command)
local f = io.popen(command, "r")
local g = grep(pattern,f)

f:close()

return(g)

end
